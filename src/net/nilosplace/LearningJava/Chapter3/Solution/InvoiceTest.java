package net.nilosplace.LearningJava.Chapter3.Solution;

public class InvoiceTest {

	public static void main(String[] args) {
		Invoice in = new Invoice();
		Invoice in2 = new Invoice();
		in.setDescription("This is the part");
		in2.setDescription("This is the part 2");
		//assert in.getDescription().equals("This is the part");
		in.setPartNumber("PartNumber");
		in2.setPartNumber("PartNumber2");
		//assert in.getPartNumber().equals("PartNumber");
		in.setQuantity(5);
		in2.setQuantity(8);
		//assert in.getQuantity() == 5;
		in.setPrice(9.99);
		in2.setPrice(2.99);
		//assert in.getPrice() == 9.99;
		
		//assert in.getInvoiceAmount() == 49.95;
		
		System.out.println("Invoice Amount: " + in.getInvoiceAmount());
		in.printInvoice();
		in2.printInvoice();
	}

}
