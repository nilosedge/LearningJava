package net.nilosplace.LearningJava.Chapter3.Solution;

public class HeartRates {

	private String firstName;
	private String lastName;
	private Date date;
	
	
	public HeartRates(String fName, String lName, int bm, int bd, int by) {
		firstName = fName;
		lastName = lName;
		date = new Date(bm, bd, by);
	}
	
	public int getAge() {
		return 2014 - date.getYear();
	}
	
	public int getMHR() {
		return 220 - getAge();
	}
	
	public double getTHRlow() {
		return .5 * getMHR();
	}
	
	public double getTHRhigh() {
		return .8 * getMHR();
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public void displayMessage() {
		System.out.println(firstName + " " + lastName);
		System.out.print("BirthDay: ");
		date.displayDate();
		System.out.println("Age: " + getAge());
		System.out.println("Max Heart Rate: " + getMHR());
		System.out.printf("Target Heart Rate: %f - %f\n", getTHRlow(), getTHRhigh());
	}
}
