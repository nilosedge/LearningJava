package net.nilosplace.LearningJava.Chapter3.Solution;

public class Account {

	private double balance;

	public Account( double initialBalance ) {
		if ( initialBalance > 0.0 )
			balance = initialBalance;
	}

	public void credit( double amount ) {
		balance = balance + amount; // add amount to balance
	}

	public double getBalance() {
		return balance; // gives the value of balance to the calling method }
	}
	
	public void debit(double amount) {
		if(balance >= amount) {
			balance -= amount;
		} else {
			System.out.println("Debit amount exceeded account balance.");
		}
	}
}