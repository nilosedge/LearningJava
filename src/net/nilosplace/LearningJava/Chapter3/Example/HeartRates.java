package net.nilosplace.LearningJava.Chapter3.Example;

public class HeartRates 
{
	private String firstName;
	private String lastName;
	int month;
	int day;
	int year;
	int currentYear;
	int personsAge;
	int maxHeartRate;
	int personsTargetHeartRate;

	public HeartRates ( String n, String lName, int m, int d, int y, int cuYear, int persAge, int mHRate, int pTHR)
	{
		firstName = n;
		lastName = lName;
		month = m;
		day = d;
		year = y;
		currentYear = cuYear;
		personsAge = persAge;
		maxHeartRate = mHRate;
		personsTargetHeartRate = pTHR;
	}

	public void setFirstName(String n)
	{
		firstName = n;
	}
	public void setLastName(String lName)
	{
		lastName = lName;
	}
	public void setMonth(int m)
	{
		month = m;
	}
	public void setDay(int d)
	{
		day = d;
	}
	public void setYear(int y)
	{
		year = y;
	}
	public void setCurrentYear(int cuYear)
	{
		currentYear = cuYear;
	}
	public void setPersonsAge(int persAge)
	{
		personsAge = persAge; 
	}
	public void setMaxHeartRate(int mHRate)
	{
		maxHeartRate = mHRate;
	}
	public void setPersonsTargetHeartRate(int pTHR)
	{
		personsTargetHeartRate = pTHR;
	}

	public String getFirstName()
	{
		return firstName;
	}
	public String getLastName()
	{
		return lastName;
	}
	public int getMonth()
	{
		return month;
	}
	public int getDay()
	{
		return day;
	} 
	public int getYear()
	{
		return year;
	}
	public int getCurrentYear()
	{
		return currentYear;
	}
	public int getPersonsAge()
	{
		return personsAge;
	}
	public int getMaxHeartRate()
	{
		return maxHeartRate;
	}
	public int getpersonsTargetHeartRate()
	{
		return personsTargetHeartRate;
	}

	public void personsAge(int personsAge)
	{
		this.personsAge = year - currentYear;
	}
	public void maxHeartRate(int maxHeartRate)
	{
		maxHeartRate = 220 - personsAge;
	}
	public void personsTargetHeartRate(int personsTargetHeartRate)
	{
		personsTargetHeartRate = maxHeartRate / 85 - 50; 
	}



}

