package net.nilosplace.LearningJava.Chapter3.Solution;

import java.util.Scanner;

public class AccountTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Account account1 = new Account( 50.00 );
		Account account2 = new Account( -7.53 );

		System.out.printf( "account1 balance: $%.2f\n", account1.getBalance() );
		System.out.printf( "account2 balance: $%.2f\n\n", account2.getBalance() );

		Scanner input = new Scanner( System.in );

		double depositAmount;

		System.out.print( "Enter deposit amount for account1: " );
		depositAmount = input.nextDouble();
		System.out.printf( "\nadding %.2f to account1 balance\n\n", depositAmount );
		account1.credit( depositAmount );
		System.out.printf( "account1 balance: $%.2f\n", account1.getBalance() );
		System.out.printf( "account2 balance: $%.2f\n\n", account2.getBalance() );

		System.out.print( "Enter deposit amount for account2: " ); // prompt
		depositAmount = input.nextDouble();
		System.out.printf( "\nadding %.2f to account2 balance\n\n", depositAmount );
		account2.credit( depositAmount ); // add to account2 balance
		// display balances
		System.out.printf( "account1 balance: $%.2f\n", account1.getBalance() );
		System.out.printf( "account2 balance: $%.2f\n", account2.getBalance() );
		
		System.out.print( "Enter depit amount for account1: " ); // prompt
		depositAmount = input.nextDouble();
		System.out.printf( "\nremoving %.2f from account1 balance\n\n", depositAmount );
		account1.debit( depositAmount ); // add to account1 balance
		
		System.out.printf( "account1 balance: $%.2f\n", account1.getBalance() );
		System.out.printf( "account2 balance: $%.2f\n", account2.getBalance() );
		
		System.out.print( "Enter depit amount for account2: " ); // prompt
		depositAmount = input.nextDouble();
		System.out.printf( "\nremoving %.2f from account2 balance\n\n", depositAmount );
		account2.debit( depositAmount ); // add to account2 balance
		
		System.out.printf( "account1 balance: $%.2f\n", account1.getBalance() );
		System.out.printf( "account2 balance: $%.2f\n", account2.getBalance() );
	}

}
