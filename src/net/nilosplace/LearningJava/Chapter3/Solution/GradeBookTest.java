package net.nilosplace.LearningJava.Chapter3.Solution;


public class GradeBookTest {

	public static void main(String[] args) {


		GradeBook gradeBook1 = new GradeBook("CS101 Introduction to Java Programming", "Olin");
		GradeBook gradeBook2 = new GradeBook("CS102 Data Structures in Java", "Olin2");
		GradeBook gradeBook3 = new GradeBook();
		
		gradeBook1.displayMessage();
		gradeBook2.displayMessage();
		gradeBook3.displayMessage();

	}

}
