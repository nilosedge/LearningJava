package net.nilosplace.LearningJava.Chapter2;

import java.util.Scanner;

public class Problem023 {
	
	/* Write statements to accomplish each of the following tasks:
		a) Declare variables c, thisIsAVariable, q76354 and number to be of type int.
		b) Prompt the user to enter an integer.
		c) Input an integer and assign the result to int variable value. Assume Scanner variable
		input can be used to read a value from the keyboard.
		d) Print "This is a Java program" on one line in the command window. Use method
		System.out.println.
		e) Print "This is a Java program" on two lines in the command window. The first line
		should end with Java. Use method System.out.println.
		f) Print "This is a Java program" on two lines in the command window. The first line
		should end with Java. Use method System.out.printf and two %s format specifiers.
		g) If the variable number is not equal to 7, display "The variable number is not equal to 7".
	 */

	public static void main(String[] args) {{
		System.out.println("This is problem 23");
		int c;
		int thisIsAVariable;
		int q76354;
		int number = 0;
		
		System.out.print("Please enter a number: ");
		Scanner input = new Scanner( System.in );
		int value = input.nextInt();
		System.out.println("This is the number that was entered: " + value);
		
		System.out.println("This is a Java program");
		
		System.out.println("This is a Java");
		System.out.println(" program");
		
		System.out.printf("%s\n%s\n", "This is a Java", " program");
		
		if(number != 7) {
			System.out.println("The variable number is not equal to 7");
		}
		
		if(number != 5) {
			System.out.println("The variable number is not equal to 5");
		}
		
		if(number == 5) {
			System.out.println("This is number");
		}
		
	}

	}}
