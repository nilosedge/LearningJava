package net.nilosplace.LearningJava.ScreenBounce;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class ScreenBounceJPanel extends JPanel {

	private ArrayList<Pencil> pens = new ArrayList<Pencil>();
	private int width = 0;
	private int height = 0;


	public ScreenBounceJPanel(int width, int height) {
		this.width = width;
		this.height = height;
		setBackground(Color.WHITE);
		for(int i = 0; i < 10000; i++) {
			pens.add(new Pencil());
		}
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		movePen();
		g.setColor(Color.WHITE);
		g.clearRect(0, 0, width, height);
		g.setColor(Color.BLACK);
		for(Pencil pen: pens) {
			g.fillRect((int)pen.x, (int)pen.y, (int)pen.xs, (int)pen.ys);
		}
	}

	private void movePen() {
		for(Pencil pen: pens) {
			pen.update(width, height);
		}
		
	}

}
