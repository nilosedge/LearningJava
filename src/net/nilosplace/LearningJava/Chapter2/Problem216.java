package net.nilosplace.LearningJava.Chapter2;

import java.util.Scanner;

public class Problem216 {

	/*
	 * (Comparing Integers) Write an application that asks the user to enter two
	 * integers, obtains them from the user and displays the larger number followed
	 * by the words "is larger". If the numbers are equal, print the message
	 * "These numbers are equal". Use the techniques shown in Fig. 2.15.
	 */
	
	public static void main(String[] args) {
	
		Scanner input = new Scanner (System.in );
		
		int number1; // First number to compare
		int number2; // Second number to compare
		
		System.out.print("enter first integer");
		number1 = input.nextInt();
		
		System.out.print("enter second integer");
		number2 = input.nextInt();
		
		if ( number1 > number2)
			System.out.printf("%d is larger", number1, number2);
		
		if ( number1 == number2) 
			System.out.printf("These numbers are equal", number1, number2 );
		
		// from here on, i went further
		
		if ( number1 < number2)
			System.out.printf("%d is larger", number2, number1 );
	}

}
