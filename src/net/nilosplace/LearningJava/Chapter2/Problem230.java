package net.nilosplace.LearningJava.Chapter2;

import java.util.Scanner;

public class Problem230 {

	/*
	 * (Separating the Digits in an Integer) Write an application that inputs one
	 * number consisting of five digits from the user, separates the number into
	 * its individual digits and prints the digits separated from one another by
	 * three spaces each. For example, if the user types in the number 42339, the
	 * program should print Assume that the user enters the correct number of digits.
	 * What happens when you execute the program and type a number with more than
	 * five digits? What happens when you execute the pro- gram and type a number
	 * with fewer than five digits? [Hint: It's possible to do this exercise with
	 * the techniques you learned in this chapter. You'll need to use both division
	 * and remainder operations to "pick off" each digit.]
	 */
	
	public static void main(String[] args) {
		
		int a, b, c, d, e, f, g, h, i, j;
		int in;
		
		Scanner input = new Scanner (System.in);
		in = input.nextInt();

		a = in / 10000;
		System.out.printf(" %d / 10000 = %d\n", in, a);
		b = in % 10000;
		System.out.printf(" %d %% 10000 = %d\n", in, b);
		c = b / 1000;
		d = b % 1000;
		e = d / 100;
		f = d % 100;
		g = f / 10;
		h = f % 10;
		
		System.out.printf("%d   %d   %d   %d   %d\n", a, c, e, g, h);
		

	}

}
