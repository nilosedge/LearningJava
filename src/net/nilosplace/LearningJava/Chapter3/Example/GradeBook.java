package net.nilosplace.LearningJava.Chapter3.Example;

public class GradeBook
{
	private String courseName;
	
	private String instructorAnyName;
	
	public GradeBook(String name)
		{courseName = name;
		instructorAnyName = name;}
	
	public void setCourseName(String name)
		{courseName = name;}
	public void setinstroctorAnyName(String name)
		{instructorAnyName = name;}
	
	public String getCourseName()
		{return courseName;}
	public String getinstructorAnyName()
		{return instructorAnyName;}
	
	public void displayMessage()
	{
		System.out.printf("Welcome to The Grade Book for CS101 Introduction to "
				+ "Java Programming\n%s!\n",
			getCourseName() );
		System.out.printf("This course is presented by: %s\n",
			getinstructorAnyName() );
		
		}

}
