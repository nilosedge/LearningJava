package net.nilosplace.LearningJava.Chapter3.Solution;

public class Employee {
	private String firstName;
	private String lastName;
	private double salary;
	
	public Employee() {
		firstName = "";
		lastName = "";
		salary = 0;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}
	
	public void displayMessage() {
		System.out.println(firstName + " " + lastName + " makes " + salary + " a year");
	}
}
