package net.nilosplace.LearningJava.Chapter2;

public class Problem024 {

	/* Identify and correct the errors in each of the following statements:
		a) if(c<7);
			System.out.println( "c is less than 7" );
		b) if ( c => 7 )
			System.out.println( "c is equal to or greater than 7" );
	 */

	public static void main(String[] args) {
		int c = 0;
		if(c<7) /* This code is getting run */;
		System.out.println( "c is less than 7" );
		

		if(c>=7) /*This code is getting run*/;
		System.out.println( "c is equal to or greater than 7" );
		
		
		if(1 > 0) {
			// DO this code
			// Do this code
			// Do this code
		}
		
		if(1 >= 0)
			/* do this code */ ;
		
	}

}