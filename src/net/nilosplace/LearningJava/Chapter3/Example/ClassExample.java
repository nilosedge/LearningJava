package net.nilosplace.LearningJava.Chapter3.Example;

import java.util.Scanner;

public class ClassExample {

	public static void main(String[] args) {

		// Primitive Types
		int i = 0;
		float f = 0;
		double d = 0;
		boolean b = true;
		
		
		// Reference Types
		Scanner s = new Scanner(System.in);
		String example = new String("This is a string");
		PersonClass person1 = new PersonClass();
		PersonClass person2 = new PersonClass();
		PersonClass person3 = new PersonClass("John", "Henry");
		
		person1.setFirstName("George");
		person1.setLastName("Williams");
		
		System.out.println("Object 1's first name is: " + person1.getFirstName());
		
		person2.setName("Fred", "Smith");
		
		person2.displayName();
		person1.displayName();
		
		
		
	}

}
