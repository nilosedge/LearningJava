package net.nilosplace.LearningJava.Chapter2;

public class Problem028 {

	/*
	 * Write Java statements that accomplish each of the following tasks:
		a) Display the message "Enter an integer: ", leaving the cursor on the same line.
		b) Assign the product of variables b and c to variable a.
		c) Use a comment to state that a program performs a sample payroll calculation.
	 */
	
	public static void main(String[] args) {

		System.out.print( "enter an integer");

		int b=3, c=2;
		int a= b*c;  // performs a sample payroll calculation
		
	}

}
