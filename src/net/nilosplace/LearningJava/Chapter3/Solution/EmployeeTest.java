package net.nilosplace.LearningJava.Chapter3.Solution;

public class EmployeeTest {

	public static void main(String[] args) {
		Employee em1 = new Employee();
		Employee em2 = new Employee();
		
		em1.setFirstName("Bob");
		em1.setLastName("Smith");
		em1.setSalary(25000);
		
		em2.setFirstName("Fred");
		em2.setLastName("Jones");
		em2.setSalary(35000);
		
		em1.displayMessage();
		em2.displayMessage();
		
		double raise1 = (em1.getSalary() * .10) + em1.getSalary();
		em1.setSalary(raise1);
		
		double raise2 = (em2.getSalary() * .10) + em2.getSalary();
		em2.setSalary(raise2);

		em1.displayMessage();
		em2.displayMessage();
		
	}

}
