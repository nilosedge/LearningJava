package net.nilosplace.LearningJava.Chapter2;

import java.util.Scanner;

public class Problem026 {

	/*
	 * Using the statements you wrote in Exercise 2.5, write a complete program that
	 * calculates and prints the product of three integers.
	 */
	
	public static void main(String[] args) {
		
		int x; // integer1
		int y; // integer2
		int z; // integer3
		
	Scanner  imput = new Scanner (System.in);	
	
	System.out.print("enter first integer");
		x = imput.nextInt();
	
	System.out.print("enter second integer");
		y = imput.nextInt();
			
	System.out.print("enter third integer");
		z = imput.nextInt();
	
	System.out.println("product : " +(x * y * z));
	}
	

}
