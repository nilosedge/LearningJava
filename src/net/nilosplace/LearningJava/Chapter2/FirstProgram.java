package net.nilosplace.LearningJava.Chapter2;

import java.util.Scanner;

public class FirstProgram {
	
//	 Github
//	 Print to the screen
//	 Show commenting - Single Line, Multi Line
//	 Declaring a class
//	 Name and Variables
//	 parts to this first program
//	 Command line compiling and running
//	 print vs println
//	 White space chars page 46
//	 System.out.printf
//	 Scanner, int's reading in text
//	 Java Doc
//	 Operators page 53
//	 Grouping with parens
//	 Relational Operators page 57
//	 if statement

	public static void main(String[] args) {

		int x = 5;  // 5,6,7,8
		double g = 6.0; // 6.5 8.8 
		float flo = 7; // 4.05 6.00007
		String str = "This is a string";
		
		System.out.println("This is line three");
		System.out.print("\tThis is line one of \"some\" text\nThis is line two of some text\n");
		System.out.println("This is line four of text.");
		System.out.println("Here is my " + x + " formatted string.");
		System.out.printf("Here is my %d formatted string. %s. %s.\n", x, str, "This is a third string");
		System.out.println("Here is my " + x + " formatted string. " + str);
		System.out.printf("%s\n%s\n", "Welcome to", "Java Programming!");
		System.out.printf("%d\n%d\n%d\n%d\n%d\n%s\n%d\n%d\n%g\n%f\n%d\n%d\n%d\n", 4,5,6,x,8,"String",7,2,g,flo,4,5,6);

		// This scans text in from the keyboard
		Scanner input = new Scanner( System.in );
		int a = input.nextInt();
		System.out.println("This was the input: " + a);

		// >, <, ==, !=, >=, <=
		
		if(a != 30) {
			// if a is less than b we are going to run this code
			System.out.println(a + " is equal to 30");
		} else {
			// else we will run this code
			System.out.println(a + " is not equal to 30");
		}
		
		System.out.println("Program continues");
		
	}
}
