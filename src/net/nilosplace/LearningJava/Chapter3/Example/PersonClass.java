package net.nilosplace.LearningJava.Chapter3.Example;

public class PersonClass {
	
	private String firstName = "";
	private String lastName = "";

	public PersonClass() {
		
	}
	
	public PersonClass(String fName, String lName) {
		firstName = fName;
		lastName = lName;
	}

	public void setFirstName(String string) {
		firstName = string;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setLastName(String string) {
		lastName = string;
	}
	
	public void setName(String fName, String lName) {
		firstName = fName;
		lastName = lName;
	}

	public void displayName() {
		System.out.println(firstName + " " + lastName);
	}

}
