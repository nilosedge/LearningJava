package net.nilosplace.LearningJava.ScreenBounce;

import java.awt.image.BufferedImage;

import javax.swing.JFrame;

public class ScreenBounce extends JFrame {

	private int width = 1000;
	private int height = 600;
	
	public ScreenBounce(String windowName) {
		super(windowName);
		init();
		while(true) {
			repaint();
//			try {
//				//Thread.sleep(3);
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
		}
	}
	
	private void init() {
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(width, height);
		ScreenBounceJPanel p = new ScreenBounceJPanel(width, height);
		add(p);
	}
}
