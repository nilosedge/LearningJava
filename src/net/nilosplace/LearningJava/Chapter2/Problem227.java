package net.nilosplace.LearningJava.Chapter2;

public class Problem227 {

	/*
	 * (Checkerboard Pattern of Asterisks) Write an application that displays
	 * a checkerboard pattern, as follows:
		* * * * * * * *   * * * * * * * *
		 * * * * * * * *   * * * * * * * *
		* * * * * * * *   * * * * * * * *
		 * * * * * * * *   * * * * * * * *
	 */
	
	public static void main(String[] args) {
		

		System.out.println("\t* * * * * * * *   * * * * * * * *");
		System.out.println("\t * * * * * * * *   * * * * * * * *");
		System.out.println("\t* * * * * * * *   * * * * * * * *");
		System.out.println("\t * * * * * * * *   * * * * * * * *");
	}

}
