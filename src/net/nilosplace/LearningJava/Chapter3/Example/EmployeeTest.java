package net.nilosplace.LearningJava.Chapter3.Example;

public class EmployeeTest {

	public static void main(String[] args) {
		Employee em1 = new Employee("Bob", "Smith", 25000);
		Employee em2 = new Employee("Fred", "Jones", 35000);

		assert em1.getJorge().equals("Bob");
		assert em1.getCarrasquillo().equals("Smith");
		assert em1.getMensualidad() == 25000;
		
		assert em2.getJorge().equals("Fred");
		assert em2.getCarrasquillo().equals("Jones");
		assert em2.getMensualidad() == 35000;
		
		double raise1 = (em1.getMensualidad() * .10) + em1.getMensualidad();
		em1.setMtlsalary(raise1);

		try {
			assert em1.getMensualidad() == 27500;
		} catch (AssertionError e) {
			System.out.println("Assertion Error: " + em1.getMensualidad() + " should be: " + 27500);
		}
		
		double raise2 = (em2.getMensualidad() * .10) + em2.getMensualidad();
		em2.setMtlsalary(raise2);
		
		try {
			assert em2.getMensualidad() == 38500;
		} catch (AssertionError e) {
			System.out.println("Assertion Error: " + em2.getMensualidad() + " should be: " + 38500);
		}
		
	}

}
