package net.nilosplace.LearningJava.Chapter3.Example;

public class Employee {
	private String Name; 
	private String Lname;
	private double Mtlsalary;

	public Employee(String Jorge, String Carrasquillo, double Mensualidad)
	{
		Name = Jorge;
		Lname = Carrasquillo;
		if (Mensualidad > 0.0)
		{Mtlsalary = Mensualidad;}
	}

	public void setName(String Jorge)
	{
		Name = Jorge;
	}
	public void setLname(String Carrasquillo)
	{
		Lname = Carrasquillo;
	}
	public void setMtlsalary(double Mtlsalary)
	{
		double Mensualidad = Mtlsalary;
	}

	public String getJorge()
	{
		return Name;
	}

	public String getCarrasquillo()
	{
		return Lname;
	}

	public double getMensualidad()
	{
		return Mtlsalary;
	}

}
