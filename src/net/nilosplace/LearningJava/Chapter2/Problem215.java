package net.nilosplace.LearningJava.Chapter2;

import java.util.Scanner;

public class Problem215 {
	
	/*
	 * (Arithmetic) Write an application that asks the user to enter two integers,
	 * obtains them from the user and prints their sum, product, difference and
	 * quotient (division). Use the techniques shown in Fig. 2.7.
	 */

	public static void main(String[] args) {
		int x,y;
		Scanner input = new Scanner ( System.in );
		System.out.print( "enter first integer: ");
		 x = input.nextInt();
		 System.out.print( "enter second integer: ");
		 y = input.nextInt();
		 System.out.println("sum : "+(x + y)); 
		 System.out.println("product : "+(x * y));
		 System.out.println("difference : "+(x - y));
		 System.out.println("quotient : "+(x / y));
		 System.out.println("mod : "+(x % y));
	}

}
