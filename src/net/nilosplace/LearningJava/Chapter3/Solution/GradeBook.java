package net.nilosplace.LearningJava.Chapter3.Solution;

public class GradeBook {

	private String courseName; 
	private String instructorName;

	public GradeBook() {
		courseName = "";
		instructorName = "";
	}
	
	public GradeBook( String cname, String iname ) {
		courseName = cname;
		instructorName = iname;
	}
	
	public void displayMessage() {
		System.out.printf( "Welcome to the grade book for\n%s!\n", getCourseName());
		System.out.println( "This course is presented by: " + getInstructorName());
	}

	public void setCourseName( String name ) {
		courseName = name;
	}

	public String getCourseName() {
		return courseName;
	}

	public String getInstructorName() {
		return instructorName;
	}

	public void setInstructorName(String name) {
		instructorName = name;
	}

}
