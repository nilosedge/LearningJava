package net.nilosplace.LearningJava.Chapter3.Solution;

public class Invoice {

	private String partNumber;
	private String description;
	private int quantity;
	private double price;
	
	public Invoice() {
		partNumber = "";
		description = "";
		quantity = 0;
		price = 0;
	}
	
	public double getInvoiceAmount() {
		if(quantity < 0) {
			quantity = 0;
		}
		return quantity * price;
	}
	
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
	public void printInvoice() {
		System.out.println("PartNumber: " + getPartNumber() + " Description: " + getDescription() + " Q: " + getQuantity() + " Price: " + getPrice() + " Invoice Total: " + getInvoiceAmount());
	}
}
