package net.nilosplace.LearningJava.ScreenBounce;

public class Pencil {

	public double x = 400;
	public double xd;
	public double y = 400;
	public double yd;
	public double xs;
	public double ys;
	public double xsp;
	public double ysp;
	
	public Pencil() {
		xs = 1;
		ys = 1;
		xd = Math.random() > .5? 1: -1;
		yd = Math.random() > .5? 1: -1;
		//xs = (int)(Math.random() * 10);
		//ys = (int)(Math.random() * 10);
	}

	public void update(int width, int height) {
		if(x > width || x < 0) xd *= -1;
		if(y > height || y < 0) yd *= -1;
		xsp = (Math.random() * 1)+2;
		ysp = (Math.random() * 1)+2;
		x = x + (xsp * xd);
		y = y + (ysp * yd);
	}
}
