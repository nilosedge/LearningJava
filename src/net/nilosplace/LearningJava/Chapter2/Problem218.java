package net.nilosplace.LearningJava.Chapter2;

public class Problem218 {

	/*
	 * (Displaying Shapes with Asterisks) Write an application that displays
	 * a box, an oval, an arrow and a diamond using asterisks (*), as follows:
			*********      ***        *       *
			*       *    *     *     ***     * *
			*       *   *       *   *****   *   *
			*       *   *       *     *    *     *
			*       *   *       *     *   *       *
			*       *   *       *     *    *     *
			*       *   *       *     *     *   *
			*       *    *     *      *      * *
			*********      ***        *       *
	 */
	
	public static void main(String[] args) {
		System.out.println("\t*********      ***        *       *\n\t*\t*    *     *"
				+ "     ***     * *\n\t*       *   *       *   *****   *   *\n"
				+ "\t*       *   *       *     *    *     *\n\t*       *   *       *"
				+ "     *   *       *\n\t*       *   *       *     *    *     *\n\t"
				+ "*       *   *       *     *     *   *\n\t*       *    *     *      *"
				+ "      * *\n\t*********      ***        *       *");
			
		
		
		//this was a good practice!!
		
		
	}

}
