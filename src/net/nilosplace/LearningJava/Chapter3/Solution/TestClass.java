package net.nilosplace.LearningJava.Chapter3.Solution;

public class TestClass {
	
	public static int var = 24;
	
	private int ivar = var * 2;
	
	
	

	public static void main(String[] args) {
		TestClass tc1 = new TestClass();
		TestClass tc2 = new TestClass();
		TestClass tc3 = new TestClass();
		
		System.out.println(tc1.ivar + " " + tc2.ivar + " " + tc3.ivar + " " + tc1.var + " " + tc2.var + " " + tc3.var);
		tc1.ivar = 5;
		TestClass.var = 1;
		System.out.println(tc1.ivar + " " + tc2.ivar + " " + tc3.ivar + " " + TestClass.var + " " + TestClass.var + " " + TestClass.var);
		tc2.ivar = 6;
		System.out.println(tc1.ivar + " " + tc2.ivar + " " + tc3.ivar + " " + TestClass.var + " " + TestClass.var + " " + TestClass.var);
		tc3.ivar = 7;
		System.out.println(tc1.ivar + " " + tc2.ivar + " " + tc3.ivar + " " + TestClass.var + " " + TestClass.var + " " + TestClass.var);
		
	}
	
	public TestClass() {
		
		ivar = 6;
	}

}