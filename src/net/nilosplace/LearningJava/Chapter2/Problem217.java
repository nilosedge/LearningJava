package net.nilosplace.LearningJava.Chapter2;

import java.util.Scanner;

public class Problem217 {

	/* 
	 * (Arithmetic, Smallest and Largest) Write an application that
	 * inputs three integers from the user and displays the sum, average,
	 * product, smallest and largest of the numbers. Use the techniques
	 * shown in Fig. 2.15. [Note: The calculation of the average in this
	 * exercise should result in an integer representation of the average.
	 * So, if the sum of the values is 7, the average should be 2, not 2.3333....]
	 */
	
	public static void main(String[] args) {
		int a, b, c;
		
		Scanner input = new Scanner ( System.in);
	
		System.out.print( "enter first integer");
			a = input.nextInt();
		
		System.out.print( "enter second integer");
			b = input.nextInt();
		
		System.out.print("enter third integer" );
			c = input.nextInt();
		
		System.out.println("sum : "+(a + b + c));
		System.out.println("product : "+(a * b * c));
		System.out.println("difference : "+(a - b - c));
		System.out.println("quotient : "+(a / b / c));
		System.out.println("mud : "+(a % b % c));

	}

}
