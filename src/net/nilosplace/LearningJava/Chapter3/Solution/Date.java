package net.nilosplace.LearningJava.Chapter3.Solution;

public class Date {

	private int month;
	private int day;
	private int year;
	
	public Date(int inmonth, int inday, int inyear) {
		month = inmonth;
		day = inday;
		year = inyear;
	}

	public void displayDate() {
		System.out.printf("%d/%d/%d\n", month, day, year);
	}
	
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		this.day = day;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
}
