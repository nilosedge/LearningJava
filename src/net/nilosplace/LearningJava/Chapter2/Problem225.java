package net.nilosplace.LearningJava.Chapter2;

import java.util.Scanner;

public class Problem225 {
	
	/*
	 * (Odd or Even) write an application that reads an integer and determines 
	 * and prints whether it's odd or even. [Hint: Use the remainder operator.
	 * an even number is a multiple of 2. Any multiple of 2 leaves a remainder
	 * of 0 when divided by 2.] 
	 */

	public static void main(String[] args) {
		
		int a;
		int b = 2;
		int mod;
		
		Scanner input = new Scanner(System.in);
		
		System.out.print("enter an integer");
		a = input.nextInt();
		
		mod = a%b;
		//System.out.printf("%d", mod);
		if(mod == 0) {
			System.out.printf("%d is even\n", a);
		} else {
			System.out.printf("%d is odd\n", a);
		}
	}

}
