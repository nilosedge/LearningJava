package net.nilosplace.LearningJava.Chapter3.Solution;

import java.util.Scanner;

public class HeartRatesTest {

	public static void main(String[] args) {
		Scanner input = new Scanner( System.in );
		
		System.out.print("Please enter your first name: ");
		String fName = input.nextLine();
		System.out.print("Please enter your last name: ");
		String lName = input.nextLine();
		System.out.print("Please enter your birth month: ");
		int bm = input.nextInt();
		System.out.print("Please enter your birth day: ");
		int bd = input.nextInt();
		System.out.print("Please enter your birth year: ");
		int by = input.nextInt();
		
		HeartRates hr = new HeartRates(fName, lName, bm, bd, by);
		
		hr.displayMessage();
	}
}
